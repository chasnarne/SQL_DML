DECLARE
   ddl_qry VARCHAR2(100);
BEGIN
FOR rec IN (SELECT sequence_name, last_number from sequence_table_map)
   LOOP
        ddl_qry := 'ALTER sequence PRI_USER. ' || rec.sequence_name || ' RESTART START WITH ' || rec.last_number;
        dbms_output.put_line(ddl_qry);
   dbms_output.put_line('ALTER SEQUENCE ' || rec.sequence_name || '  RESTART START WITH ' || rec.last_number || ';');
     EXECUTE IMMEDIATE ddl_qry;
    --update sequence_table_map set last_number = l_output where sequence_name = rec.sequence_name;
    --commit;
    dbms_output.put_line('COMMITTED;');
   END LOOP;
END;
